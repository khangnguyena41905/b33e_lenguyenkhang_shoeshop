import React, { Component } from "react";

export default class ModelCart extends Component {
  showCartItem = () => {
    return this.props.cart.map((item) => {
      return (
        <tr class="bg-white border-b dark:bg-teal-800 dark:border-teal-700">
          <th
            scope="row"
            class="py-4 px-6 font-medium text-teal-900 whitespace-nowrap dark:text-white"
          >
            {item.id}
          </th>
          <td class="py-4 px-6">{item.name}</td>
          <td class="py-4 px-6">{item.price}</td>
          <td class="py-4 px-6">
            <img class="w-32" src={item.image} alt="" />
          </td>
          <td class="py-4 px-6">
            <button
              onClick={() => {
                this.props.handleChangeQuality(item.id, 1);
              }}
              class="bg-transparent hover:bg-blue-500 text-orange-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded "
            >
              +
            </button>
            <span class="mx-1">{item.quality}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuality(item.id, -1);
              }}
              class="bg-transparent hover:bg-blue-500 text-orange-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded "
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    let { id, name, price, image } = this.props.cart;
    console.log("price: ", price);
    return (
      <div className="container mx-auto px-12 py-12">
        <div class="overflow-x-auto relative">
          <table class="w-full text-sm text-left text-teal-500 dark:text-teal-400">
            <thead class="text-xs text-teal-700 uppercase bg-teal-50 dark:bg-teal-700 dark:text-teal-400">
              <tr>
                <th scope="col" class="py-3 px-6">
                  ID
                </th>
                <th scope="col" class="py-3 px-6">
                  Name
                </th>
                <th scope="col" class="py-3 px-6">
                  Price
                </th>
                <th scope="col" class="py-3 px-6">
                  Image
                </th>
                <th scope="col" class="py-3 px-6">
                  Quality
                </th>
              </tr>
            </thead>
            <tbody>{this.showCartItem()}</tbody>
          </table>
        </div>
      </div>
    );
  }
}
