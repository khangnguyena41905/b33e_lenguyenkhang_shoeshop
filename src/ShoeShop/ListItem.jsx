import React, { Component } from "react";
import Item from "./Item";

export default class ListItem extends Component {
  renderContent = () => {
    return this.props.dataShoes.map((shoe) => {
      return <Item shoe={shoe} handleAddToCart={this.props.handleAddToCart} />;
    });
  };
  render() {
    return (
      <div className="grid grid-cols-3 gap-4 container mx-auto px-32 py-12">
        {this.renderContent()}
      </div>
    );
  }
}
