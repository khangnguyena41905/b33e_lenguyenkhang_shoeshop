import React, { Component } from "react";

export default class Item extends Component {
  render() {
    let { name, price, shortDescription, image } = this.props.shoe;
    return (
      <div className="max-w-sm rounded overflow-hidden shadow-lg ">
        <img className="w-full" src={image} alt="Sunset in the mountains" />
        <div className="px-6 py-4">
          <div className="font-bold text-xl mb-2">{name}</div>
          <p className="text-gray-700 text-base">{shortDescription}</p>
        </div>
        <div className="flex justify-between px-6 pt-4 pb-2">
          <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
            $ {price}
          </span>
          <button
            onClick={() => {
              this.props.handleAddToCart(this.props.shoe);
            }}
            className="inline-block bg-teal-500 rounded-full px-3 py-1 text-sm font-semibold text-white mr-2 mb-2"
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
