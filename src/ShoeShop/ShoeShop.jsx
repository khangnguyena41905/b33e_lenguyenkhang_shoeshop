import React, { Component } from "react";
import Header from "../Component/Header";
import { data } from "./DataShoe";
import ListItem from "./ListItem";
import ModelCart from "./ModelCart";
export default class ShoeShop extends Component {
  state = {
    dataShoes: data,
    cart: [],
  };

  handleAddToCart = (shoe) => {
    let index = this.state.cart.findIndex((cart) => {
      return shoe.id == cart.id;
    });
    let cloneCart = [...this.state.cart];
    if (index == -1) {
      let newItem = { ...shoe, quality: 1 };
      cloneCart.push(newItem);
    } else {
      cloneCart[index].quality++;
    }

    this.setState({
      cart: cloneCart,
    });
  };
  handleChangeQuality = (id, value) => {
    let index = this.state.cart.findIndex((cart) => {
      return id == cart.id;
    });
    let cloneCart = [...this.state.cart];
    cloneCart[index].quality += value;
    cloneCart[index].quality == 0 && cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };
  handlePayment = () => {
    this.setState({
      cart: [],
    });
  };
  render() {
    console.log(this.state.dataShoes);
    return (
      <div>
        <Header
          cart={this.state.cart}
          handleChangeQuality={this.handleChangeQuality}
          handlePayment={this.handlePayment}
        />
        <ListItem
          dataShoes={this.state.dataShoes}
          handleAddToCart={this.handleAddToCart}
        />
      </div>
    );
  }
}
