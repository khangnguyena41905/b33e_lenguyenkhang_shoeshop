/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/ShoeShop/ModelCart.jsx",
    "./src/Component/Header.jsx",
    "./src/ShoeShop/ShoeShop.jsx",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
